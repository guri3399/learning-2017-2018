jQuery(document).ready(function(){

	"use strict";

	$(".rotate").textrotator({
	  animation: "flip", // You can pick the way it animates when rotating through words. Options are dissolve (default), fade, flip, flipUp, flipCube, flipCubeUp and spin.
	  separator: ",", // If you don't want commas to be the separator, you can define a new separator (|, &, * etc.) by yourself using this field.
	  speed: 2000 // How many milliseconds until the next word show.
	});

	// this window will make our navbar hidden or say 
	// transparent-bg when a user scrolls to up the var top will count scrolltop() 
	// amount and reutrns a integer value we are adding add checking and removing class
	// so if our scroll amount is greater than 70 we will se navbar else it will be hidden
	$(window).scroll(function(){

		var top = $(window).scrollTop();
		if(top>=70)
		{
			$("header").addClass('transparent-bg');
		}
		else
		{
			if($("header").hasClass('transparent-bg'))
			{
				$("header").removeClass('transparent-bg');
			}
		}
	});
	// s(this) generally return jQuery object and  [0] return dom object
		$('#user-avatar-upload').on("change",function(){
		var avatarfile = $(this)[0].files[0];
		var type = avatarfile.type;
		//alert(type);
		// we need only .png or .jpg  or .jpeg means we dont need name we need only type
		//extracting substring
		var type1 = type.substring(type.indexOf("/")+1);
		//alert(type1);
		//checking the size 
		var size = avatarfile.size;
		if (type1 != "png" && type1!="jpg" && type1!="jpeg") 
		{
			alert('File type is Not Supported');
		}
		else if(size>500000)
		{
			alert('filesize should be less than 500kb');
		}
		else
		{
			// this data will be sent thorugh ajax
			var formdata = new FormData();
			//it accepts two arguments data which are we send we are appending avatar file
			formdata.append('avatar',avatarfile); //renaimg the file
			//its a new httprequest 
			var xhr = new XMLHttpRequest();
			//avatarloadedhendler is a function
			xhr.addEventListener("load",avatarloaderhandler,false);
			//it accepts method and url where we are sending tha data
			xhr.open('POST','avatarchange.php');
			xhr.send(formdata);
			//response from avatarchange.php will be stored in this file or fucntion
			function avatarloaderhandler(evt){
				//alert(evt.target.responseText); //in this response we are getting the file name with type
				$('#avatar-image-id').attr('src',evt.target.responseText);
				//here we are changing the attribute using DOM dynamically
			}
		}
	});
		//whennever a user is uplaoding an immage that image get prepend to the user gallery
		$('#new-image').on("change",function(){
		var file = $(this)[0].files[0];
		var type = file.type;
		//alert(type);
		// we need only .png or .jpg  or .jpeg means we dont need name we need only type
		//extracting substring
		var type1 = type.substring(type.indexOf("/")+1);
		//alert(type1);
		//checking the size 
		var size = file.size;
		if (type1 != "png" && type1!="jpg" && type1!="jpeg") 
		{
			alert('File type is Not Supported');
		}
		else if(size>500000)
		{
			alert('filesize should be less than 500kb');
		}
		else
		{
			// this data will be sent thorugh ajax
			var formdata = new FormData();
			//it accepts two arguments data which are we send we are appending  file
			formdata.append('file1',file); //renaimg the file
			//its a new httprequest 
			var xhr = new XMLHttpRequest();
			//loadedhendler is a function
			xhr.addEventListener("load",loaderhandler,false);
			//it accepts method and url where we are sending tha data
			xhr.open('POST','fileupload.php');
			xhr.send(formdata);
			//response from fileupload.php will be stored in this file or fucntion
			function loaderhandler(evt){
				$('#user-uploaded-pics').prepend("<div class='col-md-4'><img src="+evt.target.responseText+"></div>");
				//alert(evt.target.responseText); //in this response we are getting the file name with type
				
			}
		}
	});
});

function approvedimage(id)
{
	var rowid = "row-"+id;
	$.ajax({
		url:'approve.php',
		data:{id:id},
		type:'POST',
		success:function(result)
		{
			$('#row-'+id).hide(3000);
			alert(result);
		}
	});
}
function deleteimage(id)
{
	var rowid = "row-"+id;
	$.ajax({
		url:'delete.php',
		data:{id:id},
		type:'POST',
		success:function(result)
		{
			$('#row-'+id).hide(3000);
			alert(result);
		}
	});
}