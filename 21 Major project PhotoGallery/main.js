jQuery(document).ready(function(){

	"use strict";

	$(".rotate").textrotator({
	  animation: "flip", // You can pick the way it animates when rotating through words. Options are dissolve (default), fade, flip, flipUp, flipCube, flipCubeUp and spin.
	  separator: ",", // If you don't want commas to be the separator, you can define a new separator (|, &, * etc.) by yourself using this field.
	  speed: 2000 // How many milliseconds until the next word show.
	});

	// this window will make our navbar hidden or say 
	// transparent-bg when a user scrolls to up the var top will count scrolltop() 
	// amount and reutrns a integer value we are adding add checking and removing class
	// so if our scroll amount is greater than 70 we will se navbar else it will be hidden
	$(window).scroll(function(){

		var top = $(window).scrollTop();
		if(top>=70)
		{
			$("header").addClass('transparent-bg');
		}
		else
		{
			if($("header").hasClass('transparent-bg'))
			{
				$("header").removeClass('transparent-bg');
			}
		}
	});

});