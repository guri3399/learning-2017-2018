<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <pre>
    --$x	Pre-decrement	Decrements $x by one, then returns $x	
    $x--	Post-decrement	Returns $x, then decrements $x by one
    </pre>
    <?php
        $x = 5 ;
        echo $x-- . "<br>";
        echo $x . "<br>";
        echo --$x ;
    ?>
</body>
</html>