<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
//Indexed Arrey
$cars = array("Volvo", "BMW", "Toyota");
$arrlength = count($cars);
print_r($cars); echo "<br>";
for($x = 0; $x < $arrlength; $x++) {
    echo $cars[$x];
    echo "<br>";
}


//Associative array
$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
print_r($age); echo "<br>";
foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
}
//Multidimensional
echo "<br>";
$cars = array
  (
  array("Volvo",22,18),
  array("BMW",15,13),
  array("Saab",5,2),
  array("Land Rover",17,15)
  );
print_r($cars);
echo "<br>"; 
for ($row = 0; $row < 4; $row++) {
  echo "<p><b>Row number $row</b></p>";
  echo "<ul>";
  for ($col = 0; $col < 3; $col++) {
    echo "<li>".$cars[$row][$col]."</li>";
  }
  echo "</ul>";
}
?>
</body>
</html>