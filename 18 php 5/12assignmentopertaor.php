<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $x = 5 ;
        $y =  13 ;

        $z = $x + $y ;
        echo $z;
        echo "<br>";
        $x += $y; // $x = $x + $y
        echo $x;
        echo "<br>";
        $x -= $y ; // $x = $x - $y
        echo $x;
        echo "<br>";
        $x /= $y ; // $x = $x / $y
        echo $x;
        echo "<br>";
        $x *= $y ; // $x = $x * $y
        echo $x;
        echo "<br>";
        $x %= $y ; // $x = $x % $y
        echo $x;
        echo "<br>";
    ?>
</body>
</html>