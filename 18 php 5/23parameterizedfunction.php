<?php
function familyName($name) {
    echo "$name Refsnes.<br>";
}

familyName("Jani");
familyName("Hege");
familyName("Stale");
familyName("Kai Jim");
familyName("Borge");
?>
<?php
function xfamilyName($name,$age) {
    echo "$name Refsnes and my age is $age.<br>";
}

xfamilyName("Jani",50);
xfamilyName("Hege",60);
xfamilyName("Stale",34);
xfamilyName("Kai Jim",56);
xfamilyName("Borge",55);
?>