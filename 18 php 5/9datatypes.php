<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <pre>
    PHP supports the following data types:    
        String
        Integer
        Float (floating point numbers - also called double)
        Boolean
        Array
        Object
        NULL
        Resource
</pre>
<?php
// example of integer
    $number = 5 ;
    echo "Integer :" . "$number" . "<br>";
    var_dump($number);
     echo "<br>";
     // example of string variable
    $name = "String";
    echo "String :" . "$name" . "<br>";
    var_dump($name) ; 
    echo "<br>";
    // example of float variable
    $float = -6.7348 ;
    echo "Float :" . "$float" . "<br>";
    var_dump($float) ; 
    echo "<br>";
    // example of boolean variable
    $m = true;
    $n = false;
    //can be used for conditions
    //example of array variable
    $array = array("john","mohn","don");
    print_r($array) ;
     echo "<br>";
    var_dump($array) ;
     echo "<br>";
     //example of object 
     class Car {
        function Car() {
            $this->model = "VW";
        }
    }
    
    // create an object
    $herbie = new Car();
    
    // show object properties
    echo $herbie->model . "<br> ";
    var_dump($herbie);
     //example of null variable
    $X = null;
    var_dump($x);
    echo "<br>";

    // example of resource variable
    function myfunction(){
        echo "My function";
    }
   $y = myfunction();
   echo "<br>";
   var_dump($y);
?>
</body>
</html>