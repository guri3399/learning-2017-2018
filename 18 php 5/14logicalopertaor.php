<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <pre>
    PHP Logical Operators
The PHP logical operators are used to combine conditional statements.

Operator	Name	Example	        Result	
and	        And 	$x and $y	    True if both $x and $y are true	
or	        Or	    $x or $y	    True if either $x or $y is true	
xor	        Xor 	$x xor $y	    True if either $x or $y is true, but not both	
&&	        And 	$x && $y	    True if both $x and $y are true	
||	        Or	    $x || $y	    True if either $x or $y is true	
!	        Not 	!$x             True if $x is not true
    </pre>
</body>
</html>