<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <pre>
    Operator    	Name	    Example     	Result
        +	    Addition	    $x + $y 	Sum of $x and $y	
        -	    Subtraction	    $x - $y	    Difference of $x and $y	
        *	    Multiplication	$x * $y	    Product of $x and $y	
        /	    Division	    $x / $y	    Quotient of $x and $y	
        %	    Modulus       	$x % $y	    Remainder of $x divided by $y	
        **	    Exponentiation	$x ** $y	Result of raising $x to the $y'th power
    </pre>
    <?php
    $x = 5 ;
    $y = 10;
    echo '$x = ' . $x . ' $y = ' . $y ;
    echo "<br>";
    echo  $x + $y;
    echo "<br>";
    echo $y-$x; // Subtraction
    echo "<br>";
    echo $x * $y ;
    echo "<br>";
    echo $x / $y ;
    echo "<br";
    echo $y % $x ;
    echo "<br>";
    echo "<br>";
    echo 3**2 ; //Exponentiation
    echo "<br>"; 


    ?>
</body>
</html>