<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <pre>
    .	Concatenation	$txt1 . $txt2	Concatenation of $txt1 and $txt2	
    .=	Concatenation assignment	$txt1 .= $txt2	Appends $txt2 to $txt1
    </pre>
    <?php
    $p1 = "Heloo string 1 ";
    $p1 .= "Added string 2";
    echo $p1;
    ?>
</body>
</html>